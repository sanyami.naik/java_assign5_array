package Assignments.five;

public class jaggedArray {
    public static void main(String[] args) {
        int m1[][]=new int[3][];
        m1[0]=new int[3];
        m1[0][0]=5;
        m1[0][1]=8;
        m1[0][2]=76;
        m1[1]=new int[3];
        m1[1][0]=4;
        m1[1][1]=2;
        m1[1][2]=1;
        m1[2]=new int[1];
        m1[2][0]=1;
        System.out.println("first matrix");
        for(int i=0;i< m1.length;i++)
        {
            for(int j=0;j<m1[i].length;j++)
            {
                System.out.print(m1[i][j]+" ");
            }
            System.out.println();
        }

        int m2[][]=new int[3][];
        m2[0]=new int[2];
        m2[0][0]=2;
        m2[0][1]=10;
        m2[1]=new int[1];
        m2[1][0]=3;
        m2[2]=new int[2];
        m2[2][0]=9;
        m2[2][1]=32;

        System.out.println("second matrix");
        for(int i=0;i< m2.length;i++)
        {
            for(int j=0;j<m2[i].length;j++)
            {
                System.out.print(m2[i][j]+" ");
            }
            System.out.println();
        }


        int sum[][]=new int[3][];



        for (int i=0;i< sum.length;i++)
        {
            int c=Math.max(m1[i].length,m2[i].length);
            sum[i]=new int[c];

            for (int j=0;j< sum[i].length;j++)
            {
                int min=Math.min(m1[j].length, m2[j].length);
                if(j<min)
                {
                    sum[i][j] = m1[i][j] + m2[i][j];
                }
                else if (m1[i].length> m2[i].length)
                {
                    sum[i][j]=m1[i][j];
                }
                else
                {
                    sum[i][j]=m2[i][j];
                }
            }

        }



        System.out.println("The matrix sum is");

        for(int[] i:sum)
        {
            for(int j:i)
            {
                System.out.print(j+" ");
            }
            System.out.println();
        }

    }
}



/*first matrix
5 8 76
4 2 1
1
second matrix
2 10
3
9 32
The matrix sum is
7 8 76
7 2 1
10 32

*/