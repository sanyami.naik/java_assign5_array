package Assignments.five;
import java.util.*;

public class OrderArray {

    public static void main(String[] args) {

        Integer array[]={12,34,89,65,45,23,98,56,32,95,47};

        System.out.println("Here for printing in ascending order we use the sort method of arrays class");
        Arrays.sort(array);
        for(int i:array)
        {
            System.out.print(i+ " ");
        }

        System.out.println();
        System.out.println("Here for printing in descending order ");
        for(int i=array.length-1;i>=0;i--)
        {
            System.out.print(array[i]+ " ");
        }


    }
}


/*OUTPUT

Here for printing in ascending order we use the sort method of arrays class
12 23 32 34 45 47 56 65 89 95 98
Here for printing in descending order
98 95 89 65 56 47 45 34 32 23 12
 */
