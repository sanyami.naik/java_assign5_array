package Assignments.five;

import java.util.Arrays;

public class copy {
    public static void main(String[] args) {

        int array[]={23,76,45,98,35,26,65};
        int[] copyarr=Arrays.copyOf(array,array.length);


        System.out.println("The original array is ");
        for (int i:array)
        {
            System.out.print(i+" ");
        }

        System.out.println();
        System.out.println("The copied array is ");
        for (int i:copyarr)
        {
            System.out.print(i+" ");
        }

    }

}


/*OUTPUT:

The original array is
23 76 45 98 35 26 65
The copied array is
23 76 45 98 35 26 65

 */

