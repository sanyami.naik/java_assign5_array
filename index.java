package Assignments.five;
import com.sun.xml.internal.ws.api.model.wsdl.WSDLOutput;

import java.util.*;

public class index {
    public static void main(String[] args) {
        int arr[]={10,34,64,78,23,86,65};
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the number ");
        int n = sc.nextInt();
        Arrays.sort(arr);

        for(int i:arr) {
            System.out.print(i+" ");
        }

        System.out.println();
        System.out.println("The "+n +"th highest number is "+arr[arr.length-n]);
    }
}


/*OUTPUT:

Enter the number
3
10 23 34 64 65 78 86
The 3th highest number is 65

 */