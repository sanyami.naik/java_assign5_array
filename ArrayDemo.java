package Assignments.five;

import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;

import java.util.Arrays;
import java.util.List;

public class ArrayDemo {

    public static void main(String[] args) {


        int intArr[] = {10, 20, 15, 22, 35};
        int intArr1[] = {10, 15, 22, 12};
        int intArr2[] = {10, 20, 15, 22, 35};

        //aslist
        List l = Arrays.asList(intArr);
        System.out.println("Integer Array as List: " + l.toString());


        //binarySearch
        int key = 15;
        System.out.println("the value " + key + " is at index " + Arrays.binarySearch(intArr, key));
        System.out.println("the value " + key + " is at index " + Arrays.binarySearch(intArr, 2, 3, key));



        //equals
        System.out.println("The equality of arrays intArr and intArr1 is " + Arrays.equals(intArr, intArr1));
        System.out.println("The equality of arrays intArr and intArr2 is " + Arrays.equals(intArr2, intArr));



        //sort
        //int[] sort=Arrays.sort(intArr2);    Sorted array also stores in that array itself and only return type is void
        Arrays.sort(intArr2);
        System.out.println("The sorted array is arrays is " + Arrays.toString(intArr2)); //for printing the elements


        //copyOf
        System.out.println("The copy of is arrays is " + Arrays.toString(Arrays.copyOf(intArr2, 4))); //for printing the elements .This works on already sorted arrays


        //fill
        Arrays.fill(intArr1,10);
        System.out.println("Printing using a for each loop here");
        for (int i:intArr1)
        {
            System.out.println(i);
        }


    }
}




/*OUTPUT:
Integer Array as List: [[I@1b6d3586]
the value 15 is at index 2
the value 15 is at index 2
The equality of arrays intArr and intArr1 is false
The equality of arrays intArr and intArr2 is true
The sorted array is arrays is [10, 15, 20, 22, 35]
The copy of is arrays is [10, 15, 20, 22]
Printing using a for each loop here
10
10
10
10


 */